const express = require('express');
const mailer = require('./nodemailer');
const bodyParser = require('body-parser');

//telegramBot
//const telegramBot = require('node-telegram-bot-api');
//const TOKEN = '';
//const ID = '-';
//const bot = new telegramBot(TOKEN,{polling:true});
//bot.sendMessage(ID,"text");

const app = express();
const PORT = process.env.PORT || 3000;

app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use(bodyParser.urlencoded({extended: false}));
let user = undefined;

app.post('/registration', (req, res) => {
  if (!req.body.email && !req.body.phone) {
    res.sendStatus(404);
  }

  const message = {
    to: "myEmail",
    subject: "ORDER FROM MY_WEB",
    html:
      `
<h1 style="color:red">We receive new DATA from USER</h1>
<ul>
<li>Login:${req.body.email}</li>
<li>Phone:${req.body.phone}</li>
<li>Text:${req.body.text}</li>
</ul>
${req.body.checkbox ? `Need a news from US!` : ''}
`}

  mailer(message);
  user = req.body;
  res.redirect("/registration")
})

app.get("/registration", (req, res) => {
  if (typeof user !== 'object') {
    return res.sendFile(__dirname + "/registration.html");
  }
  res.send("success");
  user = undefined;
})


app.listen(PORT, () => {
  console.log(`Server listening on PORT ${PORT}`);
})
