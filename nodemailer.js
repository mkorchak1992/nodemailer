const nodemailer = require('nodemailer');
const poolConfig = "smtps://email:password.rambler.ru/?pool=true";

const transporter = nodemailer.createTransport(
  {
  pool: true,
  host: "smtp.rambler.ru",
  port: 465,
  secure: true,
  auth: {
    user:"email",
    pass:"password"
  }
},
  {
    from: "MY_WEB_PAGE <email>"
  })

const mailer = message => {
  transporter.sendMail(message, (err, data) => {
    if (err) {
      return console.log(err);
    }
    console.log(data);
  })
}

module.exports = mailer;
